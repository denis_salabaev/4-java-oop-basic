package com.example.task05;

import java.util.Objects;

/**
 * Точка в двумерном пространстве
 */
public class Point {
  private final double x;
  private final double y;

  /**
   * Конструктор, инициализирующий координаты точки
   *
   * @param x координата по оси абсцисс
   * @param y координата по оси ординат
   */
  public Point(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public Point(Point other) {
    Objects.requireNonNull(other);

    this.x = other.x;
    this.y = other.y;
  }

  /**
   * Возвращает координату точки по оси абсцисс
   *
   * @return координату точки по оси X
   */
  public double getX() {
    return x;
  }

  /**
   * Возвращает координату точки по оси ординат
   *
   * @return координату точки по оси Y
   */
  public double getY() {
    return y;
  }

  /**
   * Подсчитывает расстояние от текущей точки до точки, переданной в качестве
   * параметра
   *
   * @param other вторая точка отрезка
   * @return расстояние от текущей точки до переданной
   */
  public double getLength(Point other) {
    Objects.requireNonNull(other);

    double diff1 = (double) (other.x - x);
    double diff2 = (double) (other.y - y);

    return Math.sqrt(diff1 * diff1 + diff2 * diff2);
  }
}
