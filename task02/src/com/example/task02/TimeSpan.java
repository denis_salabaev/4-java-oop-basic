package com.example.task02;

import java.util.Objects;

public class TimeSpan {
  // Делать отдельные поля бессмысленно и создает проблемы при работе
  // с отрицательными TimeSpan
  private long span;

  public TimeSpan(int hours, int minutes, int seconds) {
    this.span = hours * 3600 + minutes * 60 + seconds;
  }

  // Какая вообще должна быть логика у сеттеров?
  // Не видел ни одного языка с сеттерами у TimeSpan.
  public void setHours(int hours) {
  }

  public void setMinutes(int minutes) {
  }

  public void setSeconds(int seconds) {
  }

  public long getHours() {
    return span / 3600;
  }

  public long getMinutes() {
    return (span % 3600) / 60;
  }

  public long getSeconds() {
    return (span % 3600) % 60;
  }

  public void add(TimeSpan other) {
    Objects.requireNonNull(other);
    this.span += other.span;
  }

  public void subtract(TimeSpan other) {
    Objects.requireNonNull(other);
    this.span -= other.span;
  }

  public String toString() {
    if (span < 0) {
      return String.format("-%d:%d:%d", getHours(), getMinutes(), getSeconds());
    }

    return String.format("%d:%d:%d", getHours(), getMinutes(), getSeconds());
  }
}
