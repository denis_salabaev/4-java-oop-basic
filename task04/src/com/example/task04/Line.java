package com.example.task04;

import java.util.Objects;

public class Line {
  private Point p1;
  private Point p2;

  public Line(Point p1, Point p2) {
    Objects.requireNonNull(p1);
    Objects.requireNonNull(p2);

    this.p1 = p1;
    this.p2 = p2;
  }

  public double length() {
    return p1.distance(p2);
  }

  public String toString() {
    return String.format("%s -> %s", p1, p2);
  }

  public Point getP1() {
    return p1;
  }

  public Point getP2() {
    return p2;
  }

  public boolean isCollinear(Point p) {
    Objects.requireNonNull(p);
    return (p.x - p1.x) * (p2.y - p1.y) == (p2.x - p1.x) * (p.y - p1.y);
  }

  public void print() {
    System.out.println(toString());
  }
}
