package com.example.task04;

import java.util.Objects;

/**
 * Класс точки на плоскости
 */
public final class Point {
  public final int x;
  public final int y;

  public Point() {
    this.x = 0;
    this.y = 0;
  }

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public double distance(Point other) {
    Objects.requireNonNull(other);

    double diff1 = (double) (other.x - x);
    double diff2 = (double) (other.y - y);

    return Math.sqrt(diff1 * diff1 + diff2 * diff2);
  }

  public String toString() {
    return String.format("(%d, %d)", x, y);
  }

  public void print() {
    System.out.println(toString());
  }
}
