package com.example.task04;

public class Task04Main {
  public static void main(String[] args) {
    Line line = new Line(new Point(1, 2), new Point(3, 4));

    Point p1 = line.getP1();
    Point p2 = line.getP2();

    System.out.printf("|%s| = %f\n", line, line.length());
    System.out.printf("|(%s, %s)| = %f\n", p1, p2, p1.distance(p2));

    Point p3 = new Point(5, 6);
    System.out.printf("%s is collinear with %s and %s: %b\n", p3, p1, p2, line.isCollinear(p3));

    Point p4 = new Point(2, 3);
    System.out.printf("%s is collinear with %s and %s: %b\n", p4, p1, p2, line.isCollinear(p4));
  }
}
