package com.example.task03;

public class Task03Main {
  public static void main(String[] args) {
    Complex a = new Complex(1, 2);
    Complex b = new Complex(3, 4);
    Complex sum = a.sum(b);

    Complex subtract = a.subtract(b);
    Complex multiply = a.multiply(b);
    Complex divide = a.divide(b);

    System.out.printf("%s + %s = %s\n", a, b, sum);
    System.out.printf("%s - %s = %s\n", a, b, subtract);
    System.out.printf("%s * %s = %s\n", a, b, multiply);
    System.out.printf("%s / %s = %s\n", a, b, divide);
  }
}
