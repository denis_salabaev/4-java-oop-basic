package com.example.task03;

import java.util.Objects;

public class Complex {
  public double re;
  public double im;

  public Complex(double re, double im) {
    this.re = re;
    this.im = im;
  }

  public Complex sum(Complex other) {
    Objects.requireNonNull(other);
    return new Complex(this.re + other.re, this.im + other.im);
  }

  public Complex subtract(Complex other) {
    Objects.requireNonNull(other);
    return new Complex(this.re - other.re, this.im - other.im);
  }

  public Complex multiply(Complex other) {
    Objects.requireNonNull(other);
    return new Complex(this.re * other.re - this.im * other.im, this.re * other.im + this.im * other.re);
  }

  public Complex divide(Complex other) {
    Objects.requireNonNull(other);

    double denominator = other.re * other.re + other.im * other.im;
    return new Complex((this.re * other.re + this.im * other.im) / denominator,
        (this.im * other.re - this.re * other.im) / denominator);
  }

  public String toString() {
    char sign = this.im < 0 ? 0 : '+';
    return String.format("%f%c%fi", this.re, sign, this.im);
  }
}
